//
//  NSWeather.swift
//  NSWeatherDisplay
//
//  Created by Shenll_IMac on 09/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import SwiftyJSON

struct NSWeather {
    
    var dateTime: String
    var title: String
    var description: String
    var temperature: String
    var humidity: String
    var weatherIcon: String
    
    /*var weatherIcon: String
    var weatherIcon: String
    var weatherIcon: String*/
    
    init(json: JSON) {
        
        self.dateTime       = json["dt"].doubleValue.convertTimeToString()
        self.humidity       = String(json["humidity"].intValue)
        self.title          = json["weather"][0]["main"].stringValue
        self.description    = json["weather"][0]["description"].stringValue
        self.weatherIcon    = json["weather"][0]["icon"].stringValue
        self.temperature    = json["temp"]["day"].stringValue
    }
}

extension Double {
    func convertTimeToString() -> String{
        let currentDateTime = NSDate(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM hh:mm"
        return dateFormatter.string(from: currentDateTime as Date)
    }
}
