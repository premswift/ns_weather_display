//
//  NSWeatherCell.swift
//  NSWeatherDisplay
//
//  Created by Shenll_IMac on 09/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import Foundation
import UIKit

class NSWeatherCell: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var temperature: UILabel!
    @IBOutlet var humidity: UILabel!
    @IBOutlet var weatherDescription: UILabel!
    @IBOutlet var weatherIcon: UIImageView!
    @IBOutlet var date: UILabel!
    
}
