
//
//  NSWeatherTableViewController.swift
//  WeatherDisplay
//
//  Created by Shenll_IMac on 09/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

class NSWeatherTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    var detailViewController: NSWeatherDetailViewController? = nil
    var selectedWeather: NSWeather? = nil
    var weatherListing = [NSWeather]() {
        didSet {
            //self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
 
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? NSWeatherDetailViewController
        }
        
        self.initLocationManager()
        self.webserviceRequestToOpenWeatherMap()
    }
    
    //
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        if(UIDevice.current.userInterfaceIdiom == .pad){
            self.splitViewController!.preferredDisplayMode = .allVisible
        }
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let controller = (segue.destination as! UINavigationController).topViewController as! NSWeatherDetailViewController
            controller.selectedWeather = selectedWeather
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if weatherListing.count > 0 {
            return weatherListing.count
        }
        return 0
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "NSWeatherCell", for: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "NSWeatherCell", for: indexPath) as! NSWeatherCell
        
        let weather = weatherListing[indexPath.row]
        cell.date?.text = weather.dateTime
        cell.title?.text = "Title : "+weather.title
        cell.temperature?.text = "Temperature : "+weather.temperature+" C"
        cell.humidity?.text = "Humidity : "+weather.humidity
        cell.weatherDescription?.text = "Description : " + weather.description
        
        cell.weatherIcon.image = UIImage(named: "applogo")
        let iconUrl = "http://openweathermap.org/img/w/"+weather.weatherIcon+".png"
        cell.weatherIcon.downloadImageFrom(link: iconUrl, contentMode: UIViewContentMode.scaleAspectFit)
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedWeather = weatherListing[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedWeather = weatherListing[indexPath.row]
        return indexPath
    }
  
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    
    func extractData(weatherData: NSData) {
        let json = try? JSONSerialization.jsonObject(with: weatherData as Data, options: []) as! NSDictionary
        
        if json != nil {
            
            print("Weather Response Success: \(json)")
            if let arrJSON   = json?["list"] {
                    self.weatherListing.removeAll()
                    let arrList = arrJSON as! NSArray
                    for index in 0...arrList.count-1 {
                        let listJson =  arrList[index] as! NSDictionary
                        let jsonObj = JSON(listJson)
                        self.weatherListing.append(NSWeather(json: jsonObj))
                    }
                    self.tableView.reloadData()
            }
            
            selectedWeather = self.weatherListing[0]
        }
        else{
            print("Error Response:")
            self.showErrorAlert(errorMessage: "Failed to retrieve response from OpenWeatherMap. Json is empty")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("didUpdateLocations")
        // Uncomment below line if you need to update weather info based on location change
        //webserviceRequestToOpenWeatherMap()
    }
    
    func webserviceRequestToOpenWeatherMap(){
        let path = "http://api.openweathermap.org/data/2.5/forecast/daily?q=Philadelphia&mode=json&units=metric&cnt=16&APPID=39485fbc112161a1c5b8fde7c1bc8ff2"
        print(path)
        let url = NSURL(string: path)
        
        let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            DispatchQueue.main.async {
                self.extractData(weatherData: data! as NSData)
            }
        }
        
        task.resume()
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        //print("didFailWithError")
    }
    
    func initLocationManager() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        startLocation = nil
        
    }
}


extension NSWeatherTableViewController {
    func showErrorAlert(errorMessage: String) {
        let alertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

