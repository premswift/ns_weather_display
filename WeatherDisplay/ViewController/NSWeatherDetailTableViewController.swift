//
//  NSWeatherDetailTableViewController.swift
//  NSWeatherDisplay
//
//  Created by Shenll_IMac on 09/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import UIKit

class NSWeatherDetailTableViewController: UITableViewController {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    var selectedWeather: NSWeather? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    //var selectedWeather: NSWeather?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        showWeatherSummaryInTableHeader()
    }
    
    func showWeatherSummaryInTableHeader(){
        
        if (selectedWeather != nil){
            
            titleLabel.text = selectedWeather?.title
            temperatureLabel.text = selectedWeather?.temperature
            humidityLabel.text = selectedWeather?.humidity
            weatherDescriptionLabel.text = selectedWeather?.description
            dateLabel.text = selectedWeather?.dateTime
            
            weatherIcon.image = UIImage(named: "applogo")
            if ((selectedWeather?.weatherIcon) != nil) {
                let iconUrl = "http://openweathermap.org/img/w/"+(selectedWeather?.weatherIcon)!+".png"
                weatherIcon.downloadImageFromWeb(link: iconUrl, contentMode: UIViewContentMode.scaleAspectFill)
            }
        }
        else{
            
        }
    }
   
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
}

extension UIImageView {
    func downloadImageFromWeb(link:String, contentMode: UIViewContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
