//
//  NSWeatherDetailViewController.swift
//  WeatherDisplay
//
//  Created by Shenll_IMac on 09/11/16.
//  Copyright © 2016 Shenll. All rights reserved.
//

import UIKit

class NSWeatherDetailViewController: UIViewController {
    
    var selectedWeather: NSWeather?{
        didSet {
            self.viewDidLoad()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "weaterDetailTableView" {
            let controller = segue.destination as! NSWeatherDetailTableViewController
            controller.selectedWeather = selectedWeather
        }
    }
}

